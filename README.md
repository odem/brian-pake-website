# Brian Pake's Website
Brian Pake is a Seattle audio engineer and musician, whose aim is to highlight his musical and engineering career by displaying his work through audio, video, and media press. The site is responsive with JavaScript fallbacks, accessibility features, and print stylings.  

September 2017

## Demo
https://www.brianpake.com

## My Roles
 - Co-Design
 - User-Experience
 - Front-End Development

## Tools/Skills Used
 - HTML5
 - CSS3
 - JavaScript
