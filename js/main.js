/**
 * @fileoverview Manages site navigation, centering 'box-grid' items,
 * selectable image button slideshow functionality, lazy loading multimedia,
 * and expanding and collapsing "more" sections.
 *
 * @author Shana Odem
 * @copyright  2017
 *
 * @param  {Object} window - browser's window containing a DOM document.
 * @param  {Object} document - DOM document containing the page contents.
 */
(function(window, document) {
  'use strict';
  /**
   * The threshold point when the site nav is less than this point, the nav
   * links become hidden behind a nav button, otherwise the links are exposed.
   * @type {Number}
   */
  var NAV_BREAK_POINT = 1127;

  /** LIbrary Functions= ==================================================== */
  /**
   * Determines if object is a valid DOM element node.
   * @param  {Object} obj - any data type object to be checked.
   *
   * @return {boolean} - TRUE if object is a DOM element node, otherwise FALSE.
   *
   * @function isElementNode
   */
  function isElementNode(obj) {
    return Boolean(obj) && typeof obj === 'object' && !Array.isArray(obj) &&
        !obj.hasOwnProperty('nodeType') && obj.nodeType === 1;
  }

  /**
   * Determines if value is of data type Number.
   * @param  {number} value - value to be checked.
   *
   * @return {boolean} - TRUE if value is data type Number, otherwise FALSE.
   *
   * @function isNumber
   */
  function isNumber(value) {
    return typeof value === 'number' && isFinite(value) &&
        !isNaN(parseFloat(value));
  }

  /**
   * Determines if value is a positive number.
   * Function Dependence: isNumber()
   * @param  {number} value - value to be checked.
   *
   * @return {boolean} - TRUE if value is a positive number or zero, otherwise
   *     FALSE.
   *
   * @function isPositiveNumber
   */
  function isPositiveNumber(value) {
    return isNumber(value) && value >= 0;
  }

  /**
   * Determines if value is an integer.
   * @param  {number} value - value to be checked.
   *
   * @return {boolean} - TRUE if value is an integer, otherwise FALSE.
   *
   * @function isInt
   */
  function isInt(value) {
    return Number.isInteger && Number.isInteger(value) ||
        typeof value === 'number' && isFinite(value) &&
        Math.floor(value) === value;
  }

  /**
   * Determines if value is a positive integer.
   * Function Dependence: isInt()
   * @param  {number} value - value to be checked.
   *
   * @return {boolean} - TRUE if value is a positive integer or zero, otherwise
   *     FALSE.
   *
   * @function isPositiveInt
   */
  function isPositiveInt(value) {
    return isInt(value) && value > -1;
  }

  /**
   * Determines if the browser is IE or Edge.
   *
   * @return {boolean} - TRUE if browser is IE or Edge, otherwise false.
   *
   * @function isMSBrowser
   */
  function isMSBrowser() {
    /**
     * Holds the browser's id/name.
     * @type {String}
     */
    var userAgent = window.navigator.userAgent;

    /** Function Start ------------------------------ */
    return userAgent.indexOf('MSIE ') > 0 || userAgent.indexOf('Trident/') > 0;
  }

  /**
  * Determines if string contains substring, not case sensitive.
  * @param  {!string} string - string to be searched.
  * @param  {!string} substring - string to search within string.
  *
  * @return {boolean} - TRUE if substring is found in string, otherwise FALSE.
  *
  * @function stringContains
  */
  function stringContains(string, substring) {
   return Boolean(string || string === '') &&
      Boolean(substring || substring === '') && typeof string === 'string' &&
      typeof substring === 'string' &&
      string.toLowerCase().indexOf(substring.toLowerCase()) !== -1;
  }

  /**
   * Disables the button the first button within container. No button disabled
   * if argument is invalid, or container doesn't have any button children.
   * Function Dependence: isElementNode()
   * @param {!Object} container - DOM element node containing the button to
   *     be disabled.
   *
   * @function disableLazyLoadButton
   */
  function disableLazyLoadButton(container) {
    /**
     * DOM button element node to be disabled.
     * @type {Object}
     */
    var button = null;

    /** Function Start ------------------------------ */
    if (container && isElementNode(container) &&
        container.getElementsByTagName('button')[0]) {
      button = container.getElementsByTagName('button')[0];
      button.disabled = true;
      button.setAttribute('aria-disabled', 'true');
    }
  }

  /**
   * Toggles the attribute between state1 and state2 for element.
   * Toggle only happens if all the preconditions met. Console message logged
   * if preconditions not met, or any of the arguments are invalid.
   * Preconditions:
   *  - element contains the attribute
   *  - state1 !== state2
   * Function Dependence: isElementNode()
   * @param  {!Object} element - element node containing the toggle attribute.
   * @param  {!string} attribute - contains the attribute's name.
   * @param  {!string} state1 - an attribute state.
   * @param  {!string} state2 - an attribute state different from state1.
   *
   * @function toggleDataAttribute
   */
  function toggleDataAttribute(element, attribute, state1, state2) {
    /**
     * New attribute value.
     * @type {String}
     */
    var newState = '';
    /**
     * Indicates if all arguments are valid.
     * @type {boolean}
     */
    var argsValid = Boolean(element && attribute && state1 && state2) &&
        typeof attribute === 'string' && typeof state1 === 'string' &&
        typeof state2 === 'string' && isElementNode(element) &&
        element.hasAttribute(attribute);

    /** Function Start ------------------------------ */
    if (argsValid) {
      newState = element.getAttribute(attribute) === state1 ? state2 : state1;
      element.setAttribute(attribute, newState);
    }
  }

  /**
   * Provides the total width of element regardless of box-sizing. Total width
   * includes inner width, left and right padding, and left and right borders.
   * If element is invalid, a console message logged and zero returned.
   * Function Dependencies:
   *  - isElementNode()
   *  - isMSBrowser()
   * @param  {!Object} element - DOM element node to get width of.
   *
   * @return {number} - total width of element. If element is invalid, zero
   *     returned.
   *
   * @function getElementWidth
   */
  function getElementWidth(element) {
    /**
     * Left padding of element.
     * @type {Number}
     */
    var paddingLeft = 0;
    /**
     * Right padding of element.
     * @type {Number}
     */
    var paddingRight = 0;
    /**
     * Left border of element.
     * @type {Number}
     */
    var borderLeft = 0;
    /**
     * Right border of element.
     * @type {Number}
     */
    var borderRight = 0;
    /**
     * Total width of element.
     * @type {Number}
     */
    var width = 0;
    /**
     * Element's box-sizing property value.
     * @type {String}
     */
    var boxSizing = '';

    /** Function Start ------------------------------ */
    if (element && isElementNode(element)) {
      width = parseInt(
          window.getComputedStyle(element).getPropertyValue('width'), 10);
      boxSizing =
          window.getComputedStyle(element).getPropertyValue('box-sizing');

      /** IE and Edge always function as if box-sizing is set to content-box,
          even if it is not. */
      if (boxSizing.toLowerCase() === 'content-box' || isMSBrowser()) {
        paddingLeft = parseInt(
            window.getComputedStyle(element).getPropertyValue('padding-left'),
            10);
        paddingRight = parseInt(
            window.getComputedStyle(element).getPropertyValue('padding-right'),
            10);

        borderLeft = parseInt(window.getComputedStyle(element).getPropertyValue(
              'border-left-width'), 10);
        borderRight = parseInt(
            window.getComputedStyle(element).getPropertyValue(
              'border-right-width'), 10);

        return width + paddingLeft + paddingRight + borderLeft + borderRight;
      } else {
        /** Box-sizing is set to border-box and non-IE and non-Edge browsers. */
        return width;
      }
    } else {
      console.log('getElementNode(): invalid argument');
      return width;
    }
  }

  /**
   * Returns the width of a grid of "boxes" based on the width, and the left
   * and right padding, border, and margin of the box. The returned width will
   * be less than or equal to maxWidth. If any of the arguments are invalid, a
   * console message is logged and zero is returned.
   * Function Dependencies:
   *  - isElementNode()
   *  - isPositiveNumber()
   * @param  {!Object} box - a single box (element node) within the grid.
   * @param  {!number} maxWidth - largest possible positive width of the grid.
   *
   * @return {number} - width of box-grid. If any of the arguments are invalid,
   *     zero returned.
   *
   * @function getBoxGridWidth
   */
  function getBoxGridWidth(box, maxWidth) {
    /**
     * Left margin of box.
     * @type {Number}
     */
    var boxMarginLeft = 0;
    /**
     * Right margin of box.
     * @type {Number}
     */
    var boxMarginRight = 0;
    /**
     * Total size of the box, includes box margins.
     * @type {Number}
     */
    var boxSize = 0;
    /**
     * Total box width, excludes box margins.
     * @type {Number}
     */
    var boxWidth = 0;

    /** Function Start ------------------------------ */
    if (box && maxWidth && isElementNode(box) && isPositiveNumber(maxWidth)) {
      boxWidth = getElementWidth(box);

      if (boxWidth) {
        boxMarginLeft = parseInt(
            window.getComputedStyle(box).getPropertyValue('margin-left'), 10);
        boxMarginRight = parseInt(
            window.getComputedStyle(box).getPropertyValue('margin-right'), 10);
        boxSize = boxWidth + boxMarginLeft + boxMarginRight;

        return (boxSize > 0) ? ((Math.floor(maxWidth / boxSize)) * boxSize) : 0;
      }
    } else {
      console.log('getBoxGridWidth(): one or more invalid arguments');
      return 0;
    }
  }


  /** Project Functions= ==================================================== */
  /**
   * Toggles a "more" button 'data-state' attribute between 'pressed' and
   * 'not-pressed'. The 'aria-pressed' attribute on the button is toggled. The
   * button's text is toggled between more and less - more text displayed when
   * button is not pressed, otherwise less text displayed. No errors thrown.
   * Function Dependencies:
   *  - isElementNode()
   *  - toggleDataAttribute()
   * @param  {!Object} button - DOM element node that gets toggled.
   * @param  {!string} more - non-empty button display text when more section
   *     collapsed.
   * @param  {!string} less - non-empty button display text when more section
   *     expanded.
   *
   * @function toggleMoreButton
   */
  function toggleMoreButton(button, more, less) {
    /**
     * Indicates if arguments are valid.
     * @type {Boolean}
     */
    var argsValid = Boolean(button && more && less) &&
        typeof more === 'string' && typeof less === 'string' &&
        isElementNode(button);

    /** Function Start ------------------------------ */
    if (argsValid) {
      toggleDataAttribute(button, 'data-state', 'pressed', 'not-pressed');
      toggleDataAttribute(button, 'aria-pressed', 'true', 'false');
      button.innerHTML = button.innerHTML === less ? more : less;
    }
  }

  /**
   * Toggles a "more" section and associated "more" button by setting the data
   * attribute 'data-state' on full, and if present, preview to 'expanded' or
   * 'collapsed'. If no preview is desired, set preview to falsey. @see
   * {@link toggleMoreButton} for further details on how button is toggled.
   * No errors thrown.
   * Function dependencies:
   *  - isElementNode()
   *  - toggleMoreButton()
   * @param  {!Object} button - DOM element node that gets toggled.
   * @param  {!Object} full - DOM element node whose content gets expanded and
   *     collapsed.
   * @param  {!Object} [preview] - DOM element node whose content gets
   *     expanded and collapsed.
   * @param  {!string} more  - non-empty button display text when more section
   *     collapsed.
   * @param  {!string} less - non-empty button display text when more section
   *     expanded.
   *
   * @function toggleMoreSection
   */
  function toggleMoreSection(button, full, preview, more, less) {
    /**
     * Indicates if required arguments are valid.
     * @type {Boolean}
     */
    var reqArgsValid = Boolean(button && full && more && less) &&
        typeof more === 'string' && typeof less === 'string' &&
        isElementNode(button) && isElementNode(full) &&
        button.hasAttribute('data-state');
    /**
     * Indicates if preview argument is valid.
     * @type {Boolean}
     */
    var previewValid = Boolean(preview) && isElementNode(preview);

    /** Function Start ------------------------------ */
    if (reqArgsValid) {
      toggleMoreButton(button, more, less);

      if (button.getAttribute('data-state') === 'pressed') {
        full.setAttribute('data-state', 'expanded');

        if (previewValid) {
          preview.setAttribute('data-state', 'collapsed');
        }
      } else {
        full.setAttribute('data-state', 'collapsed');

        if (previewValid) {
          preview.setAttribute('data-state', 'expanded');
        }
      }
    }
  }

  /**
   * Determines if text of <p> elements within container contains more
   * characters than specified by numChar. If any of the arguments is invalid,
   * FALSE returned, and a console message logged.
   * Function Dependencies:
   *  - isElementNode()
   *  - isPositiveInt()
   * @param  {!Object}  container - DOM element node containing one or more
   *     <p> element nodes.
   * @param  {!number}  numChar - number of characters to check text against.
   *
   * @return {Boolean} - TRUE if text contains less than or equal to numChar
   *     amount of characters, otherwise FALSE.
   *
   * @function isTextLongerThan
   */
  function isTextLongerThan(container, numChar) {
    /**
     * Collection of DOM <p> element nodes whoses length to check.
     * @type {Object}
     */
    var textParagraphs = null;
    /**
     * Text within paragraph element whose length to check.
     * @type {String}
     */
    var paragraphText = '';
    /**
     * Trimeed paragraph text.
     * @type {String}
     */
    var paragraphString = '';

    /** Function Start ------------------------------ */
    if (container && numChar && isElementNode(container) &&
        isPositiveInt(numChar)) {
      textParagraphs = container.getElementsByTagName('p');

      if (textParagraphs) {
        for (var i = 0; i < textParagraphs.length; i++) {
          paragraphText = textParagraphs[i].innerText;
          /** Replaces extra whitespace within text with a single space, and
              removes extra whitespace at the beginning and end of text. */
          paragraphString =
              paragraphText.replace( /\s\s+/g, ' ').replace(/^\s+|\s+/g, '');

          if (paragraphString.length >= numChar) {
            return true;
          }

          if (paragraphString.length < numChar) {
            numChar -= paragraphString.length;
          }
        }
      }
    } else {
      console.log('isTextLongerThan: one or more invalid parameters.');
    }

    return false;
  }

  /**
   * Gets a copy of the text within the container, but the number of characters
   * in the copy is at most maxNumChar. The reduced copy of text will retain
   * any <p> elements that are within the character range [0 - maxNumChar].
   * If the reduced copy of text contains more than one <p> element, the last
   * <p> will contain atleast lineMinChar number of characters. Any empty <p>
   * elements within the character range [0 - maxNumChar] will be retained in
   * the reduced copy of text, and will not affect the number of copied
   * characters. If any of the arguments are invalid, empty array returned, and
   * a console message logged.
   * Function Dependencies:
   *  - isElementNode()
   *  - isPositiveInt()
   * @param  {!Object} container - DOM element node containing one ore more
   *     <p> nodes.
   * @param  {!number} maxNumChar - positive integer, including zero, max
   *     number of characters allowed in copied text.
   * @param  {!number} lineMinChar - positive integer, including zero, min
   *     number of characters the last paragraph will have.
   *
   * @return {Array} - non-empty array of <p> nodes copied from <p> nodes within
   *     container. Empty array returned if any of the arguments is invalid, or
   *     innerText of all <p> elements within container are empty, or only
   *     whitespace.
   *
   * @function getReducedText
   */
  function getReducedText(container, maxNumChar, lineMinChar) {
    /**
     * Collection of DOM <p> element nodes.
     * @type {Object}
     */
    var paragraphs = null;
    /**
     * Amount of paragraphs within container.
     * @type {Number}
     */
    var numParagraphs = 0;
    /**
     * Untrimmed paragraph text.
     * @type {String}
     */
    var paragraphText = '';
    /**
     * Trimmed paragraph text.
     * @type {String}
     */
    var paragraphString = '';
    /**
     * DOM element node containing preview text.
     * @type {Object}
     */
    var previewParagraph = null;
    /**
     * Contains paragraphs to be included in preview.
     * @type {Array}
     */
    var previewArray = [];
    /**
     * Current index value of paragraph collection.
     * @type {Number}
     */
    var pIndex = 0;
    /**
     * Current index value of returned array.
     * @type {Number}
     */
    var previewIndex = -1;
    /**
     * Coung of characters from end of paragraph.
     * @type {Number}
     */
    var count = 0;
    /**
     * Indicates if arguments are valid.
     * @type {Boolean}
     */
    var argsValid = Boolean(container && maxNumChar && lineMinChar) &&
        isElementNode(container) && isPositiveInt(maxNumChar) &&
        isPositiveInt(lineMinChar);

    /** Function Start ------------------------------ */
    if (argsValid) {
      paragraphs = container.getElementsByTagName('p');
      numParagraphs = (paragraphs.length || 0);

      if (paragraphs) {
        while (maxNumChar > 0 && pIndex < numParagraphs) {
          paragraphText = paragraphs[pIndex].innerText;
          /** Replaces extra whitespace within text with a single space, and
              removes extra whitespace at the beginning and end of text. */
          paragraphString =
              paragraphText.replace(/\s\s+/g, ' ').replace(/^\s+|\s+$/g, '');
          previewIndex++;

          if (paragraphString.length <= maxNumChar) {
            /** Copies paragraph node. */
            previewParagraph = paragraphs[pIndex].cloneNode(true);
            maxNumChar -= paragraphString.length;
            pIndex++;

            if (paragraphs.length > 1 && paragraphString.length > 0 &&
                maxNumChar !== 0 && maxNumChar < lineMinChar) {
              previewParagraph.innerHTML = previewParagraph.innerHTML + ' ...';
              maxNumChar = 0;
            }
          } else {
            /** Paragraphy length is longer than maxNumChar. */
            /** Starting at end of paragraph, searches for first space to
                copy all text before the space. */
            while (maxNumChar > count) {
              if (paragraphString[maxNumChar - count] === ' ') {
                previewParagraph = document.createElement('p');
                previewParagraph.innerHTML =
                    paragraphString.substring(0, (maxNumChar - count)) + ' ...';
                maxNumChar = 0;
              } else {
                count++;
              }
            }
          }

          previewArray[previewIndex] = previewParagraph;
        }
      }
    } else {
      console.log('getReducedText: one or more invalid parameters.');
    }

    return previewArray;
  }

  /**
   * Creates a bandcamp iframe, and adds it to the DOM as the last child of
   * container.
   * Precondition: audioID form: 'track=123456789' or 'album=123456789'
   * @param  {!Object} container - DOM element node which an iframe will be
   *     as the last child.
   * @param  {!string} audioID - non-empty audio identification.
   *
   * @function loadAudio
   */
  function loadAudio(container, audioID) {
    /**
     * DOM iframe element node to be attached to container as the last child.
     * @type {Object}
     */
    var iframe = null;

    /** Function Start ------------------------------ */
    if (container && audioID && typeof audioID === 'string' &&
        isElementNode(container)) {
      iframe = document.createElement('iframe');
      iframe.innerHTML = '';
      iframe.setAttribute('src', 'https://bandcamp.com/EmbeddedPlayer/' +
          audioID + '/size=small/bgcol=ffffff/linkcol=aa723a/artwork=none/' +
          'transparent=true');
      container.appendChild(iframe);
    } else {
      console.log('loadAudio: one or more invalid parameters.');
    }
  }

  /**
   * Lazy loads youtube and vimeo video iframes. Initially, the video thumbnail
   * is loaded into the video spot instead of the video. The video thumbnail is
   * shown until associated video button selected. Youtube and Vimeo allow
   * larger sized videos to auto play, smaller sized videos will not auto play.
   * Once a video is loaded, the associated video button is disabled. The
   * selector identifies all the video containers to be lazy loaded. The video
   * container must have a 'data-embed' attribute set to the video's unique
   * third-party identification. If the argument is invalid, no videos are
   * lazy loaded, and a console message logged.
   * Function Dependencies:
   *  - isElementNode()
   *  - disabledLazyLoadButton()
   *  - stringContains()
   * @param  {!string} selector - either a class or id that identifies all the
   *     video containers to be lazy loaded, and must contain the substring
   *     'youtube' or 'vimeo' (case-insensitive).
   *
   * @function lazyLoadVideos
   */
  function lazyLoadVideos(selector) {
    /**
     * Collection of DOM element nodes containing videos to be lazy loaded.
     * @type {Object}
     */
    var videos = null;
    /**
     * DOM element node which iframe will be attached as the last child.
     * @type {Object}
     */
    var video = null;
    /**
     * Number of videos to be loaded.
     * @type {Number}
     */
    var numVideos = 0;
    /**
     * Unqiue third-party video identification.
     * @type {String}
     */
    var videoID = '';
    /**
     * DOM element node containing video thumbnail.
     * @type {Object}
     */
    var image = null;
    /**
     * The source of the image associated with the video.
     * @type {String}
     */
    var imageSource = '';
    /**
     * DOM button element node associated with video.
     * @type {Object}
     */
    var videoLoadButton = null;
    /**
     * Indicates if arguments are valid.
     * @type {Boolean}
     */
    var argsValid = Boolean(selector) && typeof selector === 'string' &&
        (stringContains(selector, 'youtube') ||
        stringContains(selector, 'vimeo'));

    /**
     * Loads a youtube or vimeo video iframe within video as the last child.
     * The associated video load button is disabled.
     * Function Dependencies:
     *  - isElementNode()
     *  - disabledLazyLoadButton()
     *  - stringContains()
     * Parent Function: lazyLoadVideos
     * @param  {!Object} video - DOM element node which an iframe will be
     *     attached as the last child.
     *
     * @function loadVideo
     * @inner
     */
    function loadVideo(video) {
      /**
       * DOM iframe element node which gets attached to video as the last child.
       * @type {Object}
       */
      var iframe = null;
      /**
       * Unique video identification.
       * @type {String}
       */
      var videoTrack = '';

      /** Function Start ------------------------------ */
      if (video && isElementNode(video) && video.hasAttribute('data-embed')) {
        videoTrack = video.getAttribute('data-embed');
        disableLazyLoadButton(video);
        video.setAttribute('data-state', 'video-loaded');
        iframe = document.createElement('iframe');

        if (stringContains(selector, 'youtube')) {
          iframe.setAttribute('src', 'https://www.youtube-nocookie.com/' +
              'embed/' + videoTrack + '?rel=0&amp;showinfo=0&amp;autoplay=1');
        }

        if (stringContains(selector, 'vimeo')) {
          iframe.setAttribute('src', 'https://player.vimeo.com/video/' +
              videoTrack + '?autoplay=1&title=0&byline=0&portrait=0');
        }

        iframe.setAttribute('frameborder', '0');
        iframe.setAttribute('allowfullscreen', '');
        iframe.setAttribute('webkitallowfullscreen', '');
        iframe.setAttribute('mozallowfullscreen', '');
        video.innerHTML = '';
        video.appendChild(iframe);
      } else {
        console.log('loadVideo: invalid parameter.');
      }
    }

    /** Function Start ------------------------------ */
    if (argsValid) {
      videos = (document.getElementById(selector) ||
          document.getElementsByClassName(selector));

      if (videos) {
        numVideos = (videos.length || 1);

        for (var i = 0; i < numVideos; i++) {
          if (numVideos === 1) {
            video = videos;
          } else {
            video = videos[i];
          }

          videoID = video.getAttribute('data-embed');

          if (stringContains(selector, 'youtube')) {
            imageSource = 'https://img.youtube.com/vi/' + videoID + '/default.jpg';
          }

          if (stringContains(selector, 'vimeo')) {
            imageSource = '../images/thumbnails/' + videoID + '_720.jpg';
          }

          image = new Image();
          image.src = imageSource;
          video.appendChild(image);

          if (document.addEventListener) {
            videoLoadButton = video.getElementsByTagName('button')[0];

            if (videoLoadButton) {
              videoLoadButton.addEventListener('click',
                  loadVideo.bind(null, video), false);
              video.addEventListener('click',
                  loadVideo.bind(null, video), false);
            }
          }
        }
      } else {
        console.log('lazyLoadVideos: no element nodes containing ' + selector +
            'as a class or id found in DOM.');
      }
    }
  }

  /**
   * An image "slideshow" where only one "fullsize/large" image is shown out of
   * a group of fullsize images, and each image has an associated button. When
   * the associated button is selected that fullsize image is shown and any
   * previous image is hidden. The 'data-state' attribute of the fullsize images
   * is toggled between 'selected' and 'not-selected', and 'aria-pressed'
   * attribute of the associated buttons is toggled.
   * If any of the arguments are invalid, fullsize image not shown, or button
   * toggled, and a console message logged.
   * Preconditions:
   *  - imageContainer must contain the fullsize images
   *  - id for an image and its associated button must share the same root name
   *    with different suffices. For example,
   *    <button id="example-1-button"></button> and the associated image
   *    <img id="example-1-photo"> share the root "example-1"
   * Function Dependencies:
   *  - isElementNode()
   *  - toggleDataAttribute()
   * @param  {!Object} button - DOM button element node currently selected.
   * @param  {!Object} imagesContainer - DOM element node containing fullsize
   *     verison of image associated with buttton.
   * @param  {!string} buttonSuffix - button id suffix.
   * @param  {!string} imageSuffix - image id suffix associated with button.
   *
   * @function imageSlideshowWithButton
   */
  function imageSlideshowWithButton(
      button, imagesContainer, buttonSuffix, imageSuffix) {
    /**
     * Length of buttonSuffix
     * @type {Number}
     */
    var buttonSuffixLength = 0;
    /**
     * Length of imageSuffix
     * @type {Number}
     */
    var imageSuffixLength = 0;
    /**
     * Id of button.
     * @type {String}
     */
    var buttonId = '';
    /**
     * Id of image associated with button.
     * @type {String}
     */
    var imageId = '';
    /**
     * DOM element node of image associated with button.
     * @type {Object}
     */
    var selectedImage = null;
    /**
     * DOM element node of fullsize version of selected image associated with
     * button.
     * @type {Object}
     */
    var fullsizeImages = null;
    /**
     * Id of the image associated with the previously selected button
     * @type {String}
     */
    var previousSelectedImageId = '';
    /**
     * Id of previously selected button.
     * @type {String}
     */
    var previousSelectedButtonId = '';
    /**
     * DOM button element node of previously selected button.
     * @type {Object}
     */
    var previousSelectedButton = null;
    /**
     * Indicates if arguments are valid.
     * @type {Boolean}
     */
    var argsValid = Boolean(button && imagesContainer && buttonSuffix &&
        imageSuffix) && typeof buttonSuffix === 'string' &&
        typeof imageSuffix === 'string' && isElementNode(imagesContainer) &&
        isElementNode(button);

    /** Function Start ------------------------------ */
    if (argsValid) {
      buttonId = button.getAttribute('id');

      if (buttonId) {
        buttonSuffixLength = buttonSuffix.length;
        imageSuffixLength = imageSuffix.length;
        imageId = buttonId.slice(0, -1 * buttonSuffixLength) + imageSuffix;
        selectedImage = document.getElementById(imageId);
        fullsizeImages = imagesContainer.getElementsByTagName('img');

        for (var i = 0; i < fullsizeImages.length; i++) {
          if (fullsizeImages[i].getAttribute('data-state') === 'selected') {
            fullsizeImages[i].setAttribute('data-state', 'not-selected');
            previousSelectedImageId = fullsizeImages[i].getAttribute('id');

            if (previousSelectedImageId) {
              previousSelectedButtonId = previousSelectedImageId.slice(
                  0, -1 * imageSuffixLength) + buttonSuffix;
              previousSelectedButton =
                  document.getElementById(previousSelectedButtonId);

              if (previousSelectedButton) {
                previousSelectedButton.disabled = false;
                toggleDataAttribute(
                   previousSelectedButton, 'aria-pressed', 'true', 'false');
              }
            }
          }
        }

        if (selectedImage) {
          toggleDataAttribute(
             selectedImage, 'data-state', 'selected', 'not-selected');
        }

        button.disabled = true;
        toggleDataAttribute(button, 'aria-pressed', 'true', 'false');
      }
    } else {
      console.log('imageSlideshowWithButton: one or more invalid parameters.');
    }
  }

  /**
   * Makes the main-site navigation responsive to page width, page resize,
   * and orientation change. When browser width is less than NAV_BREAK_POINT,
   * nav links initialized to hidden, and nav menu button visible and unpressed;
   * otherwise nav links are initilized to visible and nav menu button hidden.
   *
   * @function responsiveSiteNav
   */
  function responsiveSiteNav() {
    /**
     * DOM button element node of site nav button.
     * @type {Object}
     */
    var siteNavButton = document.getElementById('site-nav-menu-button');
    /**
     * DOM element node containing the site nav elements.
     * @type {Object}
     */
    var siteNav = document.getElementById('site-nav');
    /**
     * DOM element node containing page content.
     * @type {Object}
     */
    var mainContent = document.getElementsByTagName('main')[0];

    /**
     * Makes the main-site navigation responsive to page width, page resize,
     * and orientation change.
     * Parent Function: responsiveSiteNav
     *
     * @function setMainNav
     * @inner
     */
    function setMainNav() {
      if (window.innerWidth >= NAV_BREAK_POINT) {
        if (siteNav.getAttribute('data-state') === 'hide') {
          siteNav.setAttribute('data-state', 'show');
        }

        if (siteNavButton.getAttribute('data-state') === 'pressed') {
          siteNavButton.setAttribute('data-state', 'not-pressed');
        }

        if (mainContent.getAttribute('data-state') === 'overlay') {
          mainContent.setAttribute('data-state', 'no-overlay');
        }
      } else {
        if ((siteNavButton.getAttribute('data-state') === 'not-pressed') &&
            (siteNav.getAttribute('data-state') !== 'hide')) {
          siteNav.setAttribute('data-state', 'hide');
        }
      }
    }

    /**
     * Toggles the main-site navigation menu open and close, and toggles the
     * navigation button.
     * Parent Function: responsiveSiteNav
     *
     * @function toggleNavButton
     * @inner
     */
    function toggleNavButton() {
      toggleDataAttribute(
          siteNavButton, 'data-state', 'pressed', 'not-pressed');
      toggleDataAttribute(siteNavButton, 'aria-pressed', 'true', 'false');

      if (siteNavButton.getAttribute('data-state') === 'pressed') {
        siteNav.setAttribute('data-state', 'show');
        mainContent.setAttribute('data-state', 'overlay');
      } else {
        siteNav.setAttribute('data-state', 'hide');
        mainContent.setAttribute('data-state', 'no-overlay');
      }
    }

    /** Function Start ------------------------------ */
    setMainNav();

    if (window.addEventListener) {
      window.addEventListener('resize', setMainNav, false);
      window.addEventListener('orientationchange', setMainNav, false);
    }

    if (document.addEventListener) {
      siteNavButton.addEventListener('click', toggleNavButton, false);
    }
  }

  /**
   * Sets the expand and collapse functionality of the bio section.
   *
   * @see {@link toggleBioSection} for further details.
   *
   * @function setBioPage
   */
  function setBioPage() {
    /**
     * DOM element node containing biography.
     * @type {Object}
     */
    var bio = document.getElementsByClassName('bio-text')[0];
    /**
     * DOM button element nodes of bio more button.
     * @type {Object}
     */
    var bioMoreButton = document.getElementById('bio-button');

    /**
     * Toggles the more button of the biography text section. When the text is
     * collapsed into a preview (more button not pressed), the button displays
     * "Read More". When the full text is expanded (more button pressed), the
     * button displays "Less".
     * Parent Function: setBioPage
     * @param  {!Object} button - DOM element node of more button for bio text
     * @param  {Object} bio - DOM element node containg the bio text to be
     *     expanded and collapsed.
     * @param  {string} more - text displayed when the more button not pressed.
     * @param  {string} less - text displayed when the more button pressed.
     *
     * @function toggleBioSection
     * @inner
     */
    function toggleBioSection(button, bio, more, less) {
      /**
       * Indicates if arguments are valid.
       * @type {Object}
       */
      var argsValid = Boolean(button && bio && more && less) &&
          typeof more === 'string' && typeof less === 'string' &&
          isElementNode(button) && isElementNode(bio);

      /** Function Start ------------------------------ */
      if (argsValid) {
        toggleDataAttribute(bio, 'data-state', 'preview', 'full');
        toggleMoreButton(button, more, less);
      } else {
        console.log('setBioPage: one or more invalid parameters.');
      }
    }

    /** Function Start ------------------------------ */
    if (document.addEventListener) {
      bioMoreButton.addEventListener('click', toggleBioSection.bind(null,
          bioMoreButton, bio, 'Read More', 'Less'), false);
    }
  }

  /**
   * Sets the Bands page functionality by creating press quote previews for
   * long quotes, and lazy loading any audio and video.
   * Function Dependencies:
   *  -
   *
   * @see {@link setBandPressSection} for further details about the preview,
   * expanding, and collapsing functionality of the press section.
   *
   * @see {@link lazyLoadBandAudio} for further details about the audio.
   *
   * @function setBandPage
   */
  function setBandPage() {
    /**
     * Max number of characters allowed in the preview.
     * @type {Number}
     */
    var previewCharMax = 300;
    /**
     * Min number of characters the last paragraph can have in the preview.
     * @type {Number}
     */
    var avgLineChar = 65;
    /**
     * Collection of DOM element node containers holding music tracks.
     * @type {Object}
     */
    var audio = document.getElementsByClassName('band-music-tracks');
    /**
     * Collection of DOM element nodes of fullsize images.
     * @type {Object}
     */
    var bandPhotos = document.getElementsByClassName('band-photos');
    /**
     * DOM element node of a music album.
     * @type {Object}
     */
    var musicAlbum = null;
    /**
     * DOM element button node of a music album.
     * @type {Object}
     */
    var musicAlbumLoadButton = null;
    /**
     * DOM element node container holding thumbnails.
     * @type {Object}
     */
    var imagesContainer = null;
    /**
     * Collection of DOM element nodes containing image thumbnails.
     * @type {Object}
     */
    var thumbnails = null;
    /**
     * DOM element button node containing the image thumbnail.
     * @type {Object}
     */
    var thumbnailButton = null;

    /**
     * Sets press sections to show full press quote if the of number characters
     * within the quote are less than the pressPreviewCharMax. If the press
     * quote contains more than pressPreviewCharMax number of characters, the
     * quote is collapsed into a "preview". The preview text will contain at
     * most pressPreviewCharMax number of characters. When a preview is created,
     * a "more" button is added below the quote element. The more button toggles
     * the expanding and collapsing of the preview and full quote. When the
     * button is not pressed, it shows 'Read More', otherwise 'Less' is shown.
     * If any of the arguments is invalid, press section not set, and a console
     * message logged. The preview will contain the same number of <p> elements
     * as the full quote within the character range [0 - pressPreviewCharMax].
     * Beyond pressPreviewCharMax number of characters, ectera dots (...) are
     * added to the preview section. If pressPreviewCharMax falls is within a
     * word, that word is not included in the preview, and the previous word is
     * followed by etctera dots. When the preview contains more than one
     * paragraph, the last paragraph in the preview will contain atleast
     * minLineChar number of characters. The preview is added to the DOM as a
     * previous sibling to the container holding the full press quote contents.
     * Function Dependencies:
     *  - isPositiveInt()
     *  - isTextLongerThan()
     *  - getReducedText()
     *  - toggleMoreSection()
     * Parent Function: setBandPage
     * @param {number} pressPreviewCharMax - positive integer, including zero,
     *     indicating max number of characters allowed in preview.
     * @param {number} minLineChar - positive integer, including zero,
     *     indicating min number of characters the last paragraph can have in
     *     the preview if preview contains more than one paragraph.
     *
     * @function setBandPressSection
     * @inner
     */
    function setBandPressSection(pressPreviewCharMax, minLineChar) {
      /**
       * Id of press quote.
       * @type {String}
       */
      var pressQuoteId = '';
      /**
       * Quote paragraphs that will be apart of the preview.
       * @type {Array}
       */
      var previewTextArray = [];
      /**
       * DOM element node containing the preview.
       * @type {Object}
       */
      var previewContainer = null;
      /**
       * DOM button element node preview more button.
       * @type {Object}
       */
      var moreButton = null;
      /**
       * Collection of DOM element nodes of press quotes.
       * @type {Object}
       */
      var fullPressQuote =
          document.getElementsByClassName('press-quote-fulltext');

      /** Function Start ------------------------------ */
      if (pressPreviewCharMax && isPositiveInt(pressPreviewCharMax)) {
        for (var i = 0; i < fullPressQuote.length; i++) {
          if (!isTextLongerThan(fullPressQuote[i], pressPreviewCharMax)) {
            fullPressQuote[i].setAttribute('data-state', 'expanded');
          } else {
            previewTextArray = getReducedText(
                fullPressQuote[i], pressPreviewCharMax, minLineChar);
            fullPressQuote[i].parentNode.insertBefore(
                document.createElement('div'), fullPressQuote[i]);
            previewContainer = fullPressQuote[i].previousElementSibling;
            previewContainer.classList.add('press-quote-preview');
            pressQuoteId = fullPressQuote[i].getAttribute('id');
            previewContainer.setAttribute('id', pressQuoteId + '-preview');
            previewContainer.setAttribute('data-state', 'expanded');

            for (var k = 0; k < previewTextArray.length; k++) {
              previewContainer.appendChild(previewTextArray[k]);
            }

            fullPressQuote[i].parentNode.insertBefore(
                document.createElement('button'),
                fullPressQuote[i].nextSibling);
            moreButton = fullPressQuote[i].nextSibling;
            moreButton.classList.add('read-more-button');
            moreButton.setAttribute('id', pressQuoteId + '-more-button');
            moreButton.setAttribute('data-state', 'not-pressed');
            moreButton.setAttribute('aria-expanded', 'false');
            moreButton.setAttribute('aria-controls', pressQuoteId);
            moreButton.innerHTML = 'Read More';

            if (document.addEventListener) {
              moreButton.addEventListener('click', toggleMoreSection.bind(null,
                  moreButton, fullPressQuote[i], previewContainer, 'Read More',
                  'Less'), false);
            }
          }
        }
      } else {
        console.log('setBandPressSection: one or more invalid parameters.');
      }
    }

    /**
     * Lazy loads audio track iframes within container when the asssociated
     * play/load button is selected. Button gets disabled once pressed.
     * Container must contain the individual audio track container elements, and
     * the class 'band-audio-track'. Each iframe is added as a child of an
     * individual audio track container element. Each audio track container must
     * contain the data attribute 'data-track' set to the unique identification
     * of the audio track. No errors thrown.
     * Function Dependencies:
     *  - isElementNode()
     *  - disableLazyLoadButton()
     *  - loadAudio()
     * Parent Function: setBandPage
     * @param  {!Object} container - DOM element node which an iframe is
     *     attached as the last child.
     *
     * @function lazyLoadBandAudio
     * @inner
     */
    function lazyLoadBandAudio(container) {
      /**
       * Collection of DOM element nodes of audio tracks.
       * @type {Object}
       */
      var audioTracks = null;
      /**
       * Unique audio track identification.
       * @type {String}
       */
      var audioID = '';

      /** Function Start ------------------------------ */
      if (container && isElementNode(container)) {
        disableLazyLoadButton(container);
        container.setAttribute('data-state', 'audio-loaded');
        audioTracks = container.getElementsByClassName('band-audio-track');

        for (var k = 0; k < audioTracks.length; k++) {
          audioTracks[k].setAttribute('data-state', 'audio-loaded');
          audioID = audioTracks[k].getAttribute('data-track');
          loadAudio(audioTracks[k], audioID);
        }
      }
    }

    /** Function Start ------------------------------ */
    setBandPressSection(previewCharMax, avgLineChar);
    lazyLoadVideos('youtube-video');

    if (document.addEventListener) {
      for (var i = 0; i < audio.length; i++) {
        musicAlbum = document.getElementById(audio[i].getAttribute('id'));
        musicAlbumLoadButton = musicAlbum.getElementsByTagName('button')[0];
        musicAlbumLoadButton.addEventListener('click',
            lazyLoadBandAudio.bind(null, musicAlbum), false);
      }

      for (var k = 0; k < bandPhotos.length; k++) {
        thumbnails = bandPhotos[k].getElementsByTagName('button');
        imagesContainer =
            bandPhotos[k].getElementsByClassName('band-photo-viewer')[0];

        for (var j = 0; j < thumbnails.length; j++) {
          thumbnailButton =
              document.getElementById(thumbnails[j].getAttribute('id'));
          thumbnailButton.addEventListener('click',
              imageSlideshowWithButton.bind(null, thumbnailButton,
              imagesContainer, 'button', 'photo'), false);
        }
      }
    }
  }

  /**
   * Sets Music Production page functionality by setting the width of the grid
   * of "work sample" boxes and associated production credits legend, setting
   * the toggle functionality for expaning and hiding "more" sections, and
   * lazy load any audio.
   * Function Dependencies:
   *  - loadAudio
   *  - toggleMoreSection()
   *
   * @function setMusicProdPage
   */
  function setMusicProdPage() {
    /**
     * Collection of DOM element nodes of music production more buttons.
     * @type {Object}
     */
    var albumMoreButtons =
        document.getElementsByClassName('music-prod-more-button');
    /**
     * Collection of DOM element node of music production audio tracks.
     * @type {Object}
     */
    var audioTracks = document.getElementsByClassName('music-prod-audio-track');
    /**
     * Music album button identification.
     * @type {String}
     */
    var albumMoreButtonId = '';
    /**
     * A single DOM element button node of music album.
     * @type {Object}
     */
    var albumMoreButton = null;
    /**
     * Music album more button indentification.
     * @type {String}
     */
    var albumMoreInfoId = '';
    /**
     * A single DOM element node music album more info.
     * @type {Object}
     */
    var albumMoreInfo = null;
    /**
     * A single DOM element node music track.
     * @type {Object}
     */
    var audioTrack = null;
    /**
     * A single DOM load element button node of a music track.
     * @type {Object}
     */
    var audioTrackLoadButton = null;

    /**
     * Sets the width of the grid of "work sample" boxes and associated
     * production credits legend. The width set is based on the available width
     * within the page, and the total width (width + border + padding + margin)
     * of a box.
     * Parent Function: setMusicProdPage
     *
     * @function setMusicProdPageWidth
     * @inner
     */
    function setMusicProdPageWidth() {
      /**
       * Width of browser.
       * @type {Number}
       */
      var browserWidth = window.innerWidth;
      /**
       * DOM element node of the music production page.
       * @type {Object}
       */
      var page = document.getElementById('music-prod-page');
      /**
       * DOM element node of music production grid.
       * @type {Object}
       */
      var grid = document.getElementsByClassName('music-prod-work')[0];
      /**
       * DOM element node of music production legend grid.
       * @type {Object}
       */
      var legend = document.getElementsByClassName('prod-credits-legend')[0];
      /**
       * DOM element node of music album in production grid.
       * @type {Object}
       */
      var gridElement =
          document.getElementsByClassName('music-prod-work-sample')[0];
      /**
       * Page width.
       * @type {Number}
       */
      var pageWidth =
          parseInt(window.getComputedStyle(page).getPropertyValue('width'), 10);
      /**
       * Page left padding.
       * @type {Number}
       */
      var pagePaddingLeft = parseInt(
          window.getComputedStyle(page).getPropertyValue('padding-left'), 10);
      /**
       * Page right padding.
       * @type {Number}
       */
      var pagePaddingRight = parseInt(
          window.getComputedStyle(page).getPropertyValue('padding-right'), 10);
      /**
       * Total width available of grid.
       * @type {Number}
       */
      var availableWidth = 0;
      /**
       * Total width of grid.
       * @type {Number}
       */
      var gridWidth = 0;

      /** Function Start ------------------------------ */
      if (browserWidth > pageWidth) {
        availableWidth = pageWidth;
      } else {
        availableWidth =
            browserWidth - (pagePaddingLeft || 0) - (pagePaddingRight || 0);
      }

      if (availableWidth > 0) {
        /** Grid width is always <= to the available width. */
        gridWidth = getBoxGridWidth(gridElement, availableWidth);
        grid.style.width = gridWidth + 'px';
        legend.style.width = gridWidth + 'px';

        if (window.addEventListener) {
          window.addEventListener('resize', setMusicProdPageWidth, false);
          window.addEventListener('orientationchange',
              setMusicProdPageWidth, false);
        }
      }
    }

    /**
     * Lazy loads audio iframe within container when the asssociated play/load
     * button is selected. Button gets disabled once pressed. Container must
     * have data attribute 'data-track' set to the unique identification of the
     * audio track. No errors thrown.
     * Function Dependencies:
     *  - isElementNode()
     *  - disableLazyLoadButton()
     *  - loadAudio()
     * Parent Function: setMusicProdPage
     * @param  {!Object} container - DOM element node which an iframe is
     *     attached as the last child.
     *
     * @function lazyLoadAudio
     * @inner
     */
    function lazyLoadAudio(container) {
      /**
       * Unique identification for audio track.
       * @type {String}
       */
      var audioID = '';

      /** Function Start ------------------------------ */
      if (container && isElementNode(container)) {
        disableLazyLoadButton(container);
        container.setAttribute('data-state', 'audio-loaded');
        audioID = container.getAttribute('data-track');
        loadAudio(container, audioID);
      }
    }

    /** Function Start ------------------------------ */
    setMusicProdPageWidth();

    if (document.addEventListener) {
      for (var k = 0; k < albumMoreButtons.length; k++) {
        albumMoreButtonId = albumMoreButtons[k].getAttribute('id');
        albumMoreInfoId = albumMoreButtonId.slice(0, -6) + 'info';
        albumMoreButton = document.getElementById(albumMoreButtonId);
        albumMoreInfo = document.getElementById(albumMoreInfoId);
        albumMoreButton.addEventListener('click', toggleMoreSection.bind(null,
            albumMoreButton, albumMoreInfo, null, 'More', 'Less'), false);
      }

      for (var i = 0; i < audioTracks.length; i++) {
        audioTrack = document.getElementById(audioTracks[i].getAttribute('id'));
        audioTrackLoadButton = audioTrack.getElementsByTagName('button')[0];
        audioTrackLoadButton.addEventListener('click',
            lazyLoadAudio.bind(null, audioTrack), false);
      }
    }
  }

  /**
   * Sets Sound Design page functionality by lazy loading any videos on page.
   *
   * @function setSoundDesignPage
   */
  function setSoundDesignPage() {
    lazyLoadVideos('vimeo-video');
  }

  if (document.addEventListener) {
    document.addEventListener('DOMContentLoaded', responsiveSiteNav, false);

    if (document.getElementById('bio-page')) {
      document.addEventListener('DOMContentLoaded', setBioPage, false);
    }

    if (document.getElementById('music-prod-page')) {
      document.addEventListener('DOMContentLoaded', setMusicProdPage, false);
    }

    if (document.getElementById('bands-page')) {
      document.addEventListener('DOMContentLoaded', setBandPage, false);
    }

    if (document.getElementById('sound-design-page')) {
      document.addEventListener('DOMContentLoaded', setSoundDesignPage, false);
    }
  }
}(window, document));
